# Undine

## About Undine

Undine is a cross-platform development environment for Drupal developers,
inspired by (and intended to emulate) the Acquia Cloud hosting platform.

Where prior versions of Undine used Vagrant, the current incarnation leverages
Docker to provide improved speed, stability and cross-platform compatibility.

## Dependencies

* Docker Engine and Docker Compose (Linux) or Docker Toolbox (Windows, Mac OS X)
* Ability to mount SMB shares (packaged with Windows, Mac OS X)

## Installation

1. Download dependencies for your host operating system. Linux users will need
both Docker engine and Docker Compose. Windows and Mac OS X users will have
everything they need by installing and setting up Docker Toolbox.

2. After installation, you will be presented with the option to start Kitematic
or the Docker Quickstart Terminal to set up your default environment. Run the
Docker Quickstart Terminal until it has finished setting up your environment,
leaving you with a brief introduction mesasge and a terminal prompt.

3. Either clone the Undine repository via git, or download the latest master
from https://bitbucket.org/stevens_devops/undine/get/master.zip, and unzip it
into the directory of your choice. This should be outside of any existing
project or repository, and may be done via the Docker Quickstart Terminal if
you so choose.

### Docker for Windows and Docker for Mac

Windows 10 and Mac OS X 10.10.3+ users may attempt to use Docker for Windows
or Docker for Mac respectively, though this is currently unsupported.

## Usage

1. Navigate to your Undine directory.
2. Start Undine in the background: `docker-compose up -d`
3. Confirm Undine is running: `docker-compose ps`
4. Stop Undine when finished: `docker-compose down`

## Where is Undine running? How is port forwarding and persistence handled?

Undine forwards ports from your Docker containers to _their host environment_.
For Windows and Mac OS X users, this is the virtual machine being managed with
`docker-machine`. For Linux users, this is your host operating system. If it
proves necessary, these ports can be overridden in a compose.override.yml file,
using Docker Compose's syntax and rules for overrides. 

The example configuration provided enables persistent databases and exposes a
Samba share for the code. Copying this file to docker-compose.override.yml will
enable these features.

Windows and Mac OS X users who need to know the IP address of their machine can
find it via `docker-machine ip`.

## Viewing Your Website

### Windows and Mac OS X

1. Use `docker-machine ip` to determine the IP address of your Undine instance.
2. While Undine is running, visit the same IP in your browser of choice.

### Linux

Linux users will simply be able to use localhost by default, as all of the port
forwarding happens on their native host environment.

## Adding Your Code

To ensure speed and reliability while keeping dependencies to a minimum, Undine
exposes a Docker volume for /var/www/html via SMB.

### On Mac OS X

1. Use `docker-machine ip` to determine the IP address of your Undine instance.
2. In the Finder menu, select Go > Connect to Server... 
3. Use "smb://192.168.XXX.YYY/wwwdata" (sans-quotes) as the Server Address.
4. When prompted, log in as a guest. 
5. The share is now mounted to /Volumes/wwwdata, and ready for use.

### On Windows

1. Use `docker-machine ip` to determine the IP address of your Undine instance.
2. Follow the usual instructions for mapping a network drive in your version.
3. Use "\\192.168.XXX.YYY\wwwdata" (sans-quotes) as the path to the server.
4. When prompted, log in as a guest. 
5. The share is now mapped as a network drive, and is ready for use.

### On Linux (using smbmount)
1. smbmount //localhost/wwwdata /path/to/mountpoint -o rw
2. The share is now mounted to /path/to/mountpoint, and is ready for use.

Note: Because they do not have a VM to contend with, Linux users may also 
attempt to use compose.override.yml to disable Samba, and instead expose the 
necessary directories as Docker volumes as they would with the -v flag in
`docker run`.

## Working With Databases

Undine manages a Percona 5.5 database housed in a persistent Docker volume.
This ensures that your database will persist, even if you shut the containers
down.

The database can be accessed using your MySQL client of choice with the
username "root" and the password "undine" on port 3306 of your Docker host
environment (localhost on Linux, `docker-machine ip` on Windows or Mac OS X).
Within your codebase, configuration files may use the hostnames 'database' or
'db' to refer to it.

If for some reason you need to rebuild the database from defaults, you may shut
down Undine, use `docker volume rm mysqldb` to remove the volume, and start it
again with `docker-compose up -d`. Note that this will _destroy_ the contents
of your database, and restore Undine to its default state.

## Working with Memcache

Undine leverages memcache 1.4.13 to provide an additional in-memory caching
layer. Because it is an in-memory, volatile cache, it does not persist as a
MySQL database would.

Memcache can be accessed directly on port 3306 of your Docker host environment
(localhost on Linux, `docker-machine ip` on Windows or Mac OS X). Within your
codebase, configuration files may use the hostname 'memcache' to refer to it.

## Working with Pa11y CI

Undine comes with a Pa11y CI container, to facilitate accessibility checking.
It can be run as though the command line utility (accepting all of the same
arguments) like so, and provides version information by default:

docker-compose run pa11y-ci

The container also accepts modifications to the hosts file via the TARGET_HOST
environment variable. This will map the value of TARGET_HOST to the same IP
address of the web container, in the event that name-based virtual hosting is
required for your setup. In this case, pass the environment variable using
the -e flag of `docker compose run`:

docker-compose run -e TARGET_HOST=www.example.com pa11y-ci

This can be combined with other pa11y-ci flags, such as using --sitemap to
provide an XML sitemap from a production host, and --sitemap-replace to swap
that hostname for the one provided via TARGET_HOST (running on your web
container).

See https://www.npmjs.com/package/pa11y-ci for more information.

## Disclaimers

Undine is a community effort, and is neither endorsed by nor affiliated with
Acquia nor the Drupal project. It is configured as a local development
environment, and contains default settings that are not appropriate for 
production use. Use at your own risk.
