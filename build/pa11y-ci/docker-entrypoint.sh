#!/bin/bash
set -eo pipefail

# Setup based on environment variables.
if [[ -z "${TARGET_HOST}" ]]; then
  echo "TARGET_HOST not set. Continuing..."
else
  echo "TARGET_HOST being written to hosts file..."
  echo "$(grep 'web ' /etc/hosts | awk '{print $1}')      $TARGET_HOST" >> /etc/hosts
fi

# Prepend pa11y-ci to command
set -- pa11y-ci "$@"

exec "$@"

